// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RocketProjectile.generated.h"

UCLASS()
class MYPROJECTLC2_API ARocketProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARocketProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
		class UProjectileMovementComponent* MovementComp;

	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* Bullet;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		TSubclassOf<class AImpactEffectActor> ImpactActor_Class;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	virtual void NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;

};
