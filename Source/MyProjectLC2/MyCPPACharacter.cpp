// Fill out your copyright notice in the Description page of Project Settings.


#include "MyCPPACharacter.h"
#include <Components/InputComponent.h>
#include <GameFramework/SpringArmComponent.h>
#include <Camera/CameraComponent.h>
#include <ConstructorHelpers.h>
#include <RotationMatrix.h>
#include <UnrealMath.h>
#include <BaseWeapon.h>
#include <VitalityComponent.h>
#include <GameFramework/Pawn.h>
#include <GameFramework/CharacterMovementComponent.h>
#include <GameFramework/Character.h>



// Sets default values
AMyCPPACharacter::AMyCPPACharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	VitalityComp = CreateDefaultSubobject<UVitalityComponent>(TEXT("VitalityComponent"));
	
	SpringArmComponent = CreateAbstractDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArmComponent->SetupAttachment(RootComponent);

	CameraComponent = CreateAbstractDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(SpringArmComponent);

	
	//Setting Character's Mesh
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SkelMesh(TEXT("SkeletalMesh'/Game/AnimStarterPack/UE4_Mannequin/Mesh/SK_Mannequin.SK_Mannequin'"));

	if (SkelMesh.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SkelMesh.Object);
	}
	
	//Rotating Mesh
	GetMesh()->AddLocalTransform(FTransform(FRotator(0, -90, 0).Quaternion(), FVector(0, 0, -90)));

	bUseControllerRotationYaw = false;

	SpringArmComponent->bInheritPitch = false;
	SpringArmComponent->bInheritRoll = false;
	SpringArmComponent->bInheritYaw = false;

	VitalityComp->OnOwnerDied.AddDynamic(this, &AMyCPPACharacter::CharacterDied);

	// Setting Animation Blueprint
	static ConstructorHelpers::FObjectFinder<UAnimBlueprint> AnimationBP(TEXT("AnimBlueprint'/Game/AnimStarterPack/UE4_Mannequin/Mesh/ABP_MyCharacter.ABP_MyCharacter'"));
	GetMesh()->SetAnimInstanceClass(AnimationBP.Object->GetAnimBlueprintGeneratedClass());

	static ConstructorHelpers::FObjectFinder<UAnimMontage> HideTakeWeapon(TEXT("AnimMontage'/Game/AnimStarterPack/Equip_Rifle_Standing_Montage.Equip_Rifle_Standing_Montage'"));
	if (HideTakeWeapon.Succeeded())
	{
		Equip_Rifle_Standing_Montage = HideTakeWeapon.Object;
	}

}

// Called when the game starts or when spawned
void AMyCPPACharacter::BeginPlay()
{
	Super::BeginPlay();

	CurrentWeapon = SpawnWeapon();
	AttachWeapon(WeaponSocket);
	
}

void AMyCPPACharacter::OnOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
}

void AMyCPPACharacter::SetCharacterMovementAfterPickUp(float Speed)
{
	Speed = GetCharacterMovement()->MaxWalkSpeed;
}

ABaseWeapon * AMyCPPACharacter::SpawnWeapon()
{
	if (WeaponToSpawn_Class)
	{
		FTransform TmpTransform = FTransform::Identity;
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;
		UE_LOG(LogTemp, Warning, TEXT("Trying to spawn weapon"));
		return Cast<ABaseWeapon>(GetWorld()->SpawnActor(WeaponToSpawn_Class, &TmpTransform, SpawnParams));
	}

	return nullptr;
}

void AMyCPPACharacter::AttachWeapon(FName SocketName)
{
	if (CurrentWeapon)
	{	
		CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, SocketName);
		/*FireMontage = CurrentWeapon->FireMontage;*/
	}
}

// Called every frame
void AMyCPPACharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	RotateMouse();

}

void AMyCPPACharacter::RotateMouse()
{
	FHitResult Hit;

	if (!VitalityComp->bIsAlive()) 
	{
		return;
	}

	if (GetWorld()->GetFirstPlayerController()->GetHitResultUnderCursor(ECC_Visibility, false, Hit))
	{
		float RotationYaw = FRotationMatrix::MakeFromX(Hit.ImpactPoint - GetActorLocation()).Rotator().Yaw;
		SetActorRotation(FRotator(0, RotationYaw, 0));
	}

}

// Called to bind functionality to input
void AMyCPPACharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AMyCPPACharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AMyCPPACharacter::MoveRight);
	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Pressed, this, &AMyCPPACharacter::StartSprint);
	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Released, this, &AMyCPPACharacter::StopSprint);
	PlayerInputComponent->BindAxis(TEXT("CameraMove"), this, &AMyCPPACharacter::CameraMove);
	PlayerInputComponent->BindAction(TEXT("Walk"), IE_Pressed, this, &AMyCPPACharacter::StartWalk);
	PlayerInputComponent->BindAction(TEXT("Walk"), IE_Released, this, &AMyCPPACharacter::StopWalk);
	PlayerInputComponent->BindAction(TEXT("Reload"), IE_Pressed, this, &AMyCPPACharacter::Reload);
	PlayerInputComponent->BindAction(TEXT("Hide/Take Weapon"), IE_Pressed, this, &AMyCPPACharacter::Equip);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &AMyCPPACharacter::StartFire);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Released, this, &AMyCPPACharacter::StopFire);

}

void AMyCPPACharacter::MoveForward(float AxisValue)
{
	AddMovementInput(FVector(1, 0, 0), AxisValue);
}

void AMyCPPACharacter::MoveRight(float AxisValue)
{
	AddMovementInput(FVector (0, 1, 0), AxisValue);
}

void AMyCPPACharacter::StartSprint()
{
	GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
}

void AMyCPPACharacter::StopSprint()
{
	GetCharacterMovement()->MaxWalkSpeed = JogSpeed;
}

void AMyCPPACharacter::StartWalk()
{
	GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
}

void AMyCPPACharacter::StopWalk()
{
	GetCharacterMovement()->MaxWalkSpeed = JogSpeed;
}

void AMyCPPACharacter::Fire()
{

}

void AMyCPPACharacter::Reload()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Reload();
	}
}

void AMyCPPACharacter::Equip()
{
	if (CurrentWeapon)
	{
		GetMesh()->GetAnimInstance()->Montage_Play(Equip_Rifle_Standing_Montage);
	}

	
}

void AMyCPPACharacter::SpeedBoostPickUp()
{
	////if ()
	//	{
	//		GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
	//	}
}

void AMyCPPACharacter::CameraMove(float AxisValue)
{
	CamZoomDestination = AxisValue * 50 + FMath::Clamp(CamZoomDestination, 1000.f, 1400.f);

	SpringArmComponent->TargetArmLength = FMath::FInterpTo(SpringArmComponent->TargetArmLength, CamZoomDestination, GetWorld()->DeltaTimeSeconds, 5.0f);
}

void AMyCPPACharacter::StartFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StartFire();
	}

	OnAmmoChanged.Broadcast();
}

void AMyCPPACharacter::StopFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StopFire();
	}
}

void AMyCPPACharacter::CharacterDied()
{
	GetCharacterMovement()->DisableMovement();
	bUseControllerRotationYaw = false;
	StopFire();
	/* To disable fire after death*/
	CurrentWeapon = nullptr;

	UE_LOG(LogTemp, Warning, TEXT("Char"));
}


