// Fill out your copyright notice in the Description page of Project Settings.


#include "AmmoPickUp.h"
#include "BaseWeapon.h"

void AAmmoPickUp::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//UE_LOG(LogTemp, Warning, TEXT("Ammo PickUp Activated"));
	
	//if (OtherActor)
	//{
	//	ABaseWeapon* BaseWeapon = Cast<ABaseWeapon>(OtherActor);
	//	int32 AmmoAfterPickUp = BaseWeapon->GetAmmoPool() + AmmoPickUpAmount;
	//	BaseWeapon->SetAmmoPool(AmmoAfterPickUp);
	//	UE_LOG(LogTemp, Warning, TEXT("%s was added %s ammo"), *OtherActor->GetName(), *FString::SanitizeFloat(AmmoAfterPickUp));
	//}

	SpawnEffects();

	Destroy();
}

void AAmmoPickUp::SpawnEffects()
{
	Super::SpawnEffects();
}
