// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseProjectile.generated.h"

DECLARE_DYNAMIC_DELEGATE_OneParam(FHitResultDelegate, FHitResult, Hit);

UCLASS()
class MYPROJECTLC2_API ABaseProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseProjectile();

	UPROPERTY()
		FHitResultDelegate OnHitFire;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
		class UProjectileMovementComponent* MovementComp;

	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* Bullet;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	virtual void NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;

};
