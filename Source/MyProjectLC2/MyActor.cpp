// Fill out your copyright notice in the Description page of Project Settings.


#include "MyActor.h"

// Sets default values
AMyActor::AMyActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();

}


// Called every frame
void AMyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	float Step = 100;

	float DistanceToMoveUp = Step * DeltaTime;

	float DistanceToMoveDown = Step * DeltaTime * -1;

	if (UpDirection == true)
	{
		AddActorWorldOffset(FVector(0, 0, DistanceToMoveUp));
		CurrentTime = CurrentTime + DeltaTime;

		if (CurrentTime >= 1)
		{
			UpDirection = false;
		}
	}

	else if (UpDirection == false)
	{
		AddActorWorldOffset(FVector(0, 0, DistanceToMoveDown));
		CurrentTime = CurrentTime - DeltaTime;

		if (CurrentTime <= 0)
		{
			UpDirection = true;
		}
	}

}

