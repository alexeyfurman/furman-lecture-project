// Fill out your copyright notice in the Description page of Project Settings.

#include "Weapon_Trace_Base.h"
#include <Kismet/KismetSystemLibrary.h>
#include "CollisionQueryParams.h"
#include "WorldCollision.h"
#include "DrawDebugHelpers.h"
#include "BaseWeapon.h"
#include <Runtime\Engine\Public\Collision.h>
#include "ImpactEffectComponent.h"
#include <Kismet/GameplayStatics.h>
#include <Particles/ParticleSystemComponent.h>

void AWeapon_Trace_Base::Fire()
{
	Super::Fire();
	
	TArray<FHitResult> Hits;

	FVector TraceStart = GetMuzzleTransform().GetLocation();
	FVector TraceEnd = GetMuzzleTransform().GetRotation().GetForwardVector() * TraceLength + TraceStart;

	FCollisionObjectQueryParams CollisionParams;
	CollisionParams.AddObjectTypesToQuery(ECC_PhysicsBody);
	CollisionParams.AddObjectTypesToQuery(ECC_WorldDynamic);
	CollisionParams.AddObjectTypesToQuery(ECC_WorldStatic);

	FCollisionShape CollisionShape = FCollisionShape::MakeSphere(SphereRadius);

	GetWorld()->SweepMultiByObjectType(Hits, TraceStart, TraceEnd, FQuat::Identity, CollisionParams, CollisionShape);

	//DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Red, true);

	if (Hits.Num() > 0)
	{
		for (auto TmpHit : Hits)
		{

			//TmpHit.GetComponent()->AddImpulse(TmpHit.ImpactPoint);

			ImpactEffectComp->SpawnImpactEffect(TmpHit);

			DrawDebugSphere(GetWorld(), TmpHit.ImpactPoint, SphereRadius, 16, FColor::Blue, true);

		}
	}

	if (Beam)
	{
		UParticleSystemComponent* BeamComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Beam, FTransform::Identity);

		BeamComp->SetBeamSourcePoint(0, TraceStart, 0);
		BeamComp->SetBeamTargetPoint(0, TraceEnd, 0);
	}
}

