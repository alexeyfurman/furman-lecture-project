// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon_Rocket_Launcher.h"
#include "RocketProjectile.h"

void AWeapon_Rocket_Launcher::Fire()
{
	Super::Fire();

	//Spawn Projectile
	if (ProjectileClass)
	{
		FTransform TmpTransform = GetMuzzleTransform();

		GetWorld()->SpawnActor(ProjectileClass, &TmpTransform);

	}
}

void AWeapon_Rocket_Launcher::Reload()
{
	Super::Reload();

}