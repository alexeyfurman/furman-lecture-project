// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseWeapon.h"
#include "MyCPPACharacter.h"
#include <MessageDialog.h>
#include "ImpactEffectComponent.h"
#include <GameFramework/Actor.h>
#include "TimerManager.h"

ABaseWeapon::ABaseWeapon()
{
	GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	GetStaticMeshComponent()->SetMobility(EComponentMobility::Movable);

	ImpactEffectComp = CreateDefaultSubobject<UImpactEffectComponent>(TEXT("ImpactEffectComponent"));

}

int32 ABaseWeapon::GetAmmoPool()
{
	return AmmoPool;
}

int32 ABaseWeapon::GetAmmoClipSize()
{
	return AmmoClipSize;
}

void ABaseWeapon::SetAmmoPool(int32 Ammo)
{
	Ammo = AmmoPool;
}

void ABaseWeapon::StartFire()
{	
	if (WeaponOwner)
	{
		if (AmmoLoaded > 0)
		{
			AmmoLoaded = AmmoLoaded - 1;
			OnAmmoChangedBaseWeapon.Broadcast();
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("You ran out of ammo, try reloading!"));
			return;
		}
	}

	GetWorldTimerManager().SetTimer(AutoFire_TH, this, &ABaseWeapon::Fire, 60 / FireRate, bAutoFire, 0);
}

void ABaseWeapon::StopFire()
{

	GetWorldTimerManager().ClearTimer(AutoFire_TH);

}

void ABaseWeapon::Reload()
{
	if (AmmoPool <= 0 || AmmoLoaded >= AmmoClipSize)
	{
		return;
	}

	if (AmmoPool < (AmmoClipSize - AmmoLoaded))
	{
		AmmoLoaded = AmmoLoaded + AmmoPool;
		AmmoPool = 0;
	}
	else
	{
		AmmoPool = AmmoPool - (AmmoClipSize - AmmoLoaded);
		AmmoLoaded = AmmoClipSize;
	}
	
	if (ReloadMontage)
	{
		if (WeaponOwner)
		{
			WeaponOwner->PlayAnimMontage(ReloadMontage);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("No Owner Found"));
		}
	}
	else 
	{
		UE_LOG(LogTemp, Warning, TEXT("No Fire Montage found"));
	}
}

void ABaseWeapon::Fire()
{	
	if (FireMontage)
	{
		if (WeaponOwner)
		{
			WeaponOwner->PlayAnimMontage(FireMontage);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("No Owner Found"));
		}
	}
		else {
			UE_LOG(LogTemp, Warning, TEXT("No Fire Montage found"));
		}
}


FTransform ABaseWeapon::GetMuzzleTransform()
{	
	if (MuzzleSocketName == "None")
	{
		FMessageDialog::Debugf(NSLOCTEXT("Error", "Key", "Muzzle Socket Name Not Initialized"));
		return FTransform::Identity;
	}
	
	return GetStaticMeshComponent()->GetSocketTransform(MuzzleSocketName);
}

void ABaseWeapon::BeginPlay()
{
	Super::BeginPlay();
	
	GetMuzzleTransform();

	if (GetOwner())
	{
		WeaponOwner = Cast<ACharacter>(GetOwner());
		if (!WeaponOwner)
		{
			UE_LOG(LogTemp, Warning, TEXT("Owner is not a Character"))
		}
	}

}

