// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseWeapon.h"
#include "Weapon_Trace_Base.generated.h"

/**
 * 
 */
UCLASS(abstract)
class MYPROJECTLC2_API AWeapon_Trace_Base : public ABaseWeapon
{
	GENERATED_BODY()
	
public:
	virtual void Fire() override;

	UPROPERTY(EditDefaultsOnly, Category = "Trace")
	float TraceLength = 200;

	UPROPERTY(EditDefaultsOnly, Category = "Trace")
	float SphereRadius = 50;

	UPROPERTY(EditDefaultsOnly)
		class UParticleSystem* Beam;
};
