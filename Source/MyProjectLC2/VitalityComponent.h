// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "VitalityComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParamDelegate);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYPROJECTLC2_API UVitalityComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UVitalityComponent();

	float GetMaxHealth();
	float GetCurrentHealth();
	void SetCurrentHealth(float Health);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION()
		void DamageHandle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	/*UFUNCTION()
		void StartRegeneration();*/

	/*UFUNCTION()
		void StopRegeneration();*/

	//UPROPERTY(EditDefaultsOnly, Category = "Regeneration")
		//float RegenerationSpeed = 12;

	//UPROPERTY(EditDefaultsOnly, Category = "Regeneration")
		//bool bIsHealing = false;

	//FTimerHandle Regeneration_TH;

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "Health")
		float MaxHealth = 100;

	//UPROPERTY(EditInstanceOnly, Category = "Health")
		//float MinHealth = 0;

	UPROPERTY(BlueprintReadWrite, Category = "Health")
		float CurrentHealth = MaxHealth;

	UPROPERTY(BlueprintAssignable, EditDefaultsOnly, Category = "Delegates")
		FNoParamDelegate OnHealthChanged;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Score")
		int Score = 100;


public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
		bool bIsAlive();
		
	UPROPERTY(BlueprintAssignable, Category = "Delegates")
	FNoParamDelegate OnOwnerDied;
};
