// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePickUp.h"
#include "AmmoPickUp.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECTLC2_API AAmmoPickUp : public ABasePickUp
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, Category = "Ammo")
	int32 AmmoPickUpAmount = 30;

protected:

	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	void SpawnEffects();
	
};
