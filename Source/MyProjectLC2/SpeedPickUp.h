// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePickUp.h"
#include "GameFramework/Character.h"
#include <GameFramework/CharacterMovementComponent.h>
#include <Components/StaticMeshComponent.h>
#include "SpeedPickUp.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECTLC2_API ASpeedPickUp : public ABasePickUp
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, Category = "Speed")
	float SpeedPickUpAmount = 2000.0f;

protected:

	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	void SpawnEffects();
	
};
