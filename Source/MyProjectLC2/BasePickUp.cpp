// Fill out your copyright notice in the Description page of Project Settings.


#include "BasePickUp.h"
#include <Components/ShapeComponent.h>
#include <Components/PrimitiveComponent.h>
#include <Components/StaticMeshComponent.h>
#include <Components/BoxComponent.h>
#include <Kismet/GameplayStatics.h>

// Sets default values
ABasePickUp::ABasePickUp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PickUpRoot = CreateDefaultSubobject<USceneComponent>(TEXT("PickUpRoot"));
	RootComponent = PickUpRoot;

	PickUpMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	PickUpMesh->AttachToComponent(PickUpRoot, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	PickUpMesh->BodyInstance.SetCollisionProfileName(TEXT("OverlapAll"));

	PickUpBox = CreateDefaultSubobject<UBoxComponent>(TEXT("PickUpBox"));
	PickUpBox->SetWorldScale3D(FVector(1.0f, 1.0f, 1.0f));
	PickUpBox->SetGenerateOverlapEvents(true);
	PickUpBox->OnComponentBeginOverlap.AddDynamic(this, &ABasePickUp::OnOverlapBegin);
	PickUpBox->AttachToComponent(PickUpRoot, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

}

// Called when the game starts or when spawned
void ABasePickUp::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABasePickUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABasePickUp::SpawnEffects()
{
	if (PickUpSound)
	{
		UGameplayStatics::PlaySound2D(this, PickUpSound);
	}
	if (PickUpEffect)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, PickUpEffect, GetActorLocation(), FRotator::ZeroRotator);
	}
}

void ABasePickUp::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherActor)
	{
		UE_LOG(LogTemp, Warning, TEXT("Base PickUp Activated"));
		SpawnEffects();
		Destroy();
	}
}

