// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseWeapon.h"
#include "Weapon_Rocket_Launcher.generated.h"

/**
 * 
 */
UCLASS(abstract)
class MYPROJECTLC2_API AWeapon_Rocket_Launcher : public ABaseWeapon
{
	GENERATED_BODY()

protected:
	virtual void Fire() override;

	virtual void Reload();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class ARocketProjectile> ProjectileClass;
	
};
