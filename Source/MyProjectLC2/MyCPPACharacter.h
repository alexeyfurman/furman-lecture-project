// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include <GameFramework/CharacterMovementComponent.h>
#include <UnrealMath.h>
#include "Runtime/Engine/Classes/Animation/AnimInstance.h"
#include <Engine/BlueprintCore.h>
#include "VitalityComponent.h"
#include <DelegateCombinations.h>
#include "MyCPPACharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParamDelegateChar);

class UCameraComponent;
class USpringArmComponent;
class UCharacterMovementComponent;
class UAnimMontage;
class UAnimInstance;

UCLASS()

class MYPROJECTLC2_API AMyCPPACharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMyCPPACharacter();

	UFUNCTION()
		void SetCharacterMovementAfterPickUp(float Speed);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void MoveForward(float AxisValue);

	UFUNCTION()
	void MoveRight(float AxisValue);

	UFUNCTION()
	void StartSprint();

	UFUNCTION()
	void StopSprint();

	UFUNCTION()
	void StartWalk();

	UFUNCTION()
	void StopWalk();

	UFUNCTION()
	void Fire();

	UFUNCTION()
	void Reload();

	UFUNCTION()
	void Equip();

	UFUNCTION()
	void SpeedBoostPickUp();

	UFUNCTION()
	void CameraMove(float AxisValue);

	void StartFire();
	void StopFire();

	UFUNCTION()
	void CharacterDied();

	UFUNCTION()
	void OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
		
	class ABaseWeapon* SpawnWeapon();

	void AttachWeapon(FName SocketName);

	float CamZoomDestination;

	float SprintSpeed = 1800.f;

	float JogSpeed = 600.f;

	float WalkSpeed = 200.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UVitalityComponent* VitalityComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UCameraComponent* CameraComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USpringArmComponent* SpringArmComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UCharacterMovementComponent* CharacterMovementComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UAnimMontage* Equip_Rifle_Standing_Montage;

	UPROPERTY(EditDefaultsOnly, Category = "BaseWeapon")
		TSubclassOf<class ABaseWeapon> WeaponToSpawn_Class;

	UPROPERTY(EditDefaultsOnly, Category = "BaseWeapon")
		FName WeaponSocket;

	UPROPERTY(EditDefaultsOnly, Category = "BaseWeapon")
		FName SpineWeaponSocket;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ABaseWeapon* CurrentWeapon;

	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNoParamDelegateChar OnAmmoChanged;

	bool IsReloading;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void RotateMouse();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
