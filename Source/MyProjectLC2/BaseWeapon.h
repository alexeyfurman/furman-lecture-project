// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "MyCPPACharacter.h"
#include <Animation/AnimMontage.h>
#include "BaseWeapon.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParamDelegateBaseWeapon);


/**
 * 
 */
UCLASS(abstract)
class MYPROJECTLC2_API ABaseWeapon : public AStaticMeshActor
{
	GENERATED_BODY()

public:

	ABaseWeapon();

	int32 GetAmmoPool();

	int32 GetAmmoClipSize();

	void SetAmmoPool(int32 Ammo);

	virtual void StartFire();

	virtual void StopFire();

	virtual void Reload();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
		int32 AmmoLoaded;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
		int32 AmmoPool;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
		int32 AmmoClipSize;

	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		class UAnimMontage* FireMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		class UAnimMontage* ReloadMontage;

	bool IsReloading;

protected:

	UPROPERTY(VisibleAnywhere)
		class UImpactEffectComponent* ImpactEffectComp;

	UPROPERTY(EditDefaultsOnly, Category = "Muzzle")
		FName MuzzleSocketName;

	UPROPERTY()
		class ACharacter* WeaponOwner;

	//Shots per minute
	UPROPERTY(EditDefaultsOnly, Category = "Autofire", meta = (EditCondition = "bAutoFire"))
		float FireRate = 250;

	UPROPERTY(EditDefaultsOnly, Category = "Autofire")
		bool bAutoFire = false;

	FTimerHandle AutoFire_TH;

	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNoParamDelegateBaseWeapon OnAmmoChangedBaseWeapon;

protected:

	UFUNCTION()
		virtual void Fire();

	FTransform GetMuzzleTransform();

	virtual void BeginPlay() override;

}
;