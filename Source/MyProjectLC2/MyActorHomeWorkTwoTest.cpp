// Fill out your copyright notice in the Description page of Project Settings.


#include "MyActorHomeWorkTwoTest.h"
#include <Runtime/Engine/Classes/Engine/Engine.h>

// Sets default values
AMyActorHomeWorkTwoTest::AMyActorHomeWorkTwoTest()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMyActorHomeWorkTwoTest::BeginPlay()
{
	Super::BeginPlay();

	CheckTimeElapsed();
	
}

// Called every frame
void AMyActorHomeWorkTwoTest::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMyActorHomeWorkTwoTest::CheckTimeElapsed()
{
	double BeginTime = FPlatformTime::Seconds();
	FCollisionQueryParams Params;
	FHitResult Hit;
		for (int i = 0; i < 10000; i++)
		{
			GetWorld()->LineTraceSingleByChannel(Hit, GetActorLocation(), GetActorLocation() + (GetActorLocation().GetSafeNormal() * 500), ECC_Visibility, Params);
		}

	double EndTime = FPlatformTime::Seconds();
	double PassedTime = EndTime - BeginTime;

	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, FString::SanitizeFloat(PassedTime));
}

