// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthPickUp.h"
#include "VitalityComponent.h"

void AHealthPickUp::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("Health PickUp Activated"));

	if (OtherActor->GetComponentByClass(UVitalityComponent::StaticClass()))
	{
		UVitalityComponent* VitalityComp = Cast<UVitalityComponent>(OtherActor->GetComponentByClass(UVitalityComponent::StaticClass()));
		float HealthAfterPickUp = FMath::Min(VitalityComp->GetCurrentHealth() + HealthPickUpAmount, VitalityComp->GetMaxHealth());
		VitalityComp->SetCurrentHealth(HealthAfterPickUp);
		UE_LOG(LogTemp, Warning, TEXT("%s was recovered to %s"), *OtherActor->GetName(), *FString::SanitizeFloat(HealthAfterPickUp));
		SpawnEffects();

	}

		Destroy();
}

void AHealthPickUp::SpawnEffects()
{
	Super::SpawnEffects();
}