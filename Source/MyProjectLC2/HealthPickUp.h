// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePickUp.h"
#include "VitalityComponent.h"
#include "HealthPickUp.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECTLC2_API AHealthPickUp : public ABasePickUp
{
	GENERATED_BODY()


public:

	UPROPERTY(EditDefaultsOnly, Category = "Health")
	float HealthPickUpAmount = 30.0f;

protected:

	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
	
	void SpawnEffects();
};
