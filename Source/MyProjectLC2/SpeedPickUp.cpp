// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedPickUp.h"
#include "MYCPPACharacter.h"
#include <GameFramework/Character.h>
#include <Components/StaticMeshComponent.h>
#include <GameFramework/CharacterMovementComponent.h>

void ASpeedPickUp::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//UE_LOG(LogTemp, Warning, TEXT("Speed PickUp Activated"));

	if (OtherActor)
	{
		AMyCPPACharacter* Character = Cast<AMyCPPACharacter>(OtherActor);
		if (Character) 
		{
			float SpeedAfterPickUp = Character->GetCharacterMovement()->MaxWalkSpeed + SpeedPickUpAmount;
			Character->SetCharacterMovementAfterPickUp(SpeedAfterPickUp);
			UE_LOG(LogTemp, Warning, TEXT("%s's speed was boosted to %s"), *OtherActor->GetName(), *FString::SanitizeFloat(SpeedAfterPickUp));
			SpawnEffects();
		}

	}


	Destroy();
}

void ASpeedPickUp::SpawnEffects()
{
	Super::SpawnEffects();
}