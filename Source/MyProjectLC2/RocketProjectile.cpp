// Fill out your copyright notice in the Description page of Project Settings.


#include "RocketProjectile.h"
#include "ImpactEffectActor.h"
#include <GameFramework/ProjectileMovementComponent.h>
#include <Components/StaticMeshComponent.h>

// Sets default values
ARocketProjectile::ARocketProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MovementComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));

	Bullet = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet"));
	RootComponent = Bullet;

}

// Called when the game starts or when spawned
void ARocketProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARocketProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ARocketProjectile::NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);

	if (OtherComp)
	{
		OtherComp->AddImpulse(HitLocation, "None", true);

		FTransform SpawnTransform = FTransform::Identity;

		SpawnTransform.SetLocation(HitLocation);

		AImpactEffectActor* ImpactActor = GetWorld()->SpawnActorDeferred<AImpactEffectActor>(ImpactActor_Class, SpawnTransform);

		ImpactActor->HitInit(Hit);
		ImpactActor->FinishSpawning(SpawnTransform);
	}

	Destroy();
}

