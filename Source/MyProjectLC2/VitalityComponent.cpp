// Fill out your copyright notice in the Description page of Project Settings.


#include "VitalityComponent.h"
#include <GameFramework/Actor.h>
#include "Public/TimerManager.h"
#include <Components/ActorComponent.h>
#include "MyGameInstance.h"

// Sets default values for this component's properties
UVitalityComponent::UVitalityComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


float UVitalityComponent::GetMaxHealth()
{
	return MaxHealth;
}

float UVitalityComponent::GetCurrentHealth()
{
	return CurrentHealth;
}

void UVitalityComponent::SetCurrentHealth(float Health)
{
	Health = CurrentHealth;
}

// Called when the game starts
void UVitalityComponent::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwner())
	{
		GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UVitalityComponent::DamageHandle);
	}
	
}

void UVitalityComponent::DamageHandle(AActor * DamagedActor, float Damage, const UDamageType * DamageType, AController * InstigatedBy, AActor * DamageCauser)
{
	UE_LOG(LogTemp, Warning, TEXT("%s was damaged for %s"), *DamagedActor->GetName(), *FString::SanitizeFloat(Damage));

	if (!bIsAlive())
	{
		UE_LOG(LogTemp, Warning, TEXT("AlreadyDead"));
		return;
	}

	CurrentHealth = CurrentHealth - Damage;

	OnHealthChanged.Broadcast();

	if (!bIsAlive())
	{
		//Died
		OnOwnerDied.Broadcast();
		UMyGameInstance* MyInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
		MyInstance->AddScore(Score);
		UE_LOG(LogTemp, Warning, TEXT("OwnerDied"));
	}
}

//void UVitalityComponent::StartRegeneration()
//{
	//GetWorld()->GetTimerManager().SetTimer(Regeneration_TH, this, &UVitalityComponent::DamageHandle, 60 / RegenerationSpeed, bIsHealing);

	//if (//No damage for 10 seconds)
	//	{
	//		//Start regeneration
	//	}
	//else if (//Damage taken)
	//	{
	//		//Stop regeneration
	//	}
//}

//void UVitalityComponent::StopRegeneration()
//{
	//GetWorld()->GetTimerManager().ClearTimer(Regeneration_TH);
//}


bool UVitalityComponent::bIsAlive()
{
	return CurrentHealth > 0;
}

// Called every frame
void UVitalityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

