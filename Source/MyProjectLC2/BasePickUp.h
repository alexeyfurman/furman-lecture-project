// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <Components/SkeletalMeshComponent.h>
#include <Components/StaticMeshComponent.h>
#include <Components/BoxComponent.h>
#include "BasePickUp.generated.h"

UCLASS(abstract)
class MYPROJECTLC2_API ABasePickUp : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABasePickUp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		USceneComponent* PickUpRoot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UBoxComponent* PickUpBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* PickUpMesh;

	UPROPERTY(EditDefaultsOnly, Category = "PickUp Effects")
		class USoundBase* PickUpSound;

	UPROPERTY(EditDefaultsOnly, Category = "PickUp Effects")
		class UParticleSystem* PickUpEffect;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		virtual void SpawnEffects();


};
